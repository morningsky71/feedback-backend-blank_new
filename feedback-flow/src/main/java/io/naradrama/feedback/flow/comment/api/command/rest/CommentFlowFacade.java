/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package io.naradrama.feedback.flow.comment.api.command.rest;

import io.naradrama.feedback.flow.comment.api.command.command.*;

public interface CommentFlowFacade {
    //
    RegisterCommentCommand registerComment(RegisterCommentCommand command);
    RemoveCommentCommand removeComment(RemoveCommentCommand command);
}
