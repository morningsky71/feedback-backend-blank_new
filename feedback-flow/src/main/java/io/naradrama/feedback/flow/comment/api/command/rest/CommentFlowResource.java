/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package io.naradrama.feedback.flow.comment.api.command.rest;

import io.naradrama.feedback.flow.comment.api.command.command.*;
import io.naradrama.feedback.flow.comment.domain.logic.CommentFlowLogic;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/flow/comment-flow")
@RequiredArgsConstructor
public class CommentFlowResource implements CommentFlowFacade {
    //
    private final CommentFlowLogic commentFlowLogic;

    @Override
    @PostMapping("/register-comment")
    public RegisterCommentCommand registerComment(@RequestBody RegisterCommentCommand command) {
        //
        return commentFlowLogic.registerComment(command);
    }

    @Override
    @PostMapping("/remove-comment")
    public RemoveCommentCommand removeComment(@RequestBody RemoveCommentCommand command) {
        //
        return commentFlowLogic.removeComment(command);
    }
}
