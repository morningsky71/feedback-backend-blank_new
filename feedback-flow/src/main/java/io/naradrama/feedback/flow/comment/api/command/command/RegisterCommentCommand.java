/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package io.naradrama.feedback.flow.comment.api.command.command;

import io.naradrama.feedback.aggregate.comment.domain.entity.sdo.CommentCdo;
import io.naradrama.prologue.domain.cqrs.command.CqrsUserCommand;
import io.naradrama.prologue.util.json.JsonUtil;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class RegisterCommentCommand extends CqrsUserCommand {
    //
    private CommentCdo commentCdo;

    @Override
    public String toString() {
        //
        return toJson();
    }

    public static RegisterCommentCommand fromJson(String json) {
        //
        return JsonUtil.fromJson(json, RegisterCommentCommand.class);
    }

    public static RegisterCommentCommand sample() {
        //
        CommentCdo commentCdo = CommentCdo.sample();
        RegisterCommentCommand sample = new RegisterCommentCommand();
        sample.setCommentCdo(commentCdo);
        return sample;
    }
}
