/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package io.naradrama.feedback.flow.comment.api.command.command;

import io.naradrama.prologue.domain.cqrs.command.CqrsUserCommand;
import io.naradrama.prologue.util.json.JsonUtil;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class RemoveCommentCommand extends CqrsUserCommand {
    //
    private String commentId;

    @Override
    public String toString() {
        //
        return toJson();
    }

    public static RemoveCommentCommand fromJson(String json) {
        //
        return JsonUtil.fromJson(json, RemoveCommentCommand.class);
    }

    public static RemoveCommentCommand sample() {
        //
        String commentId = UUID.randomUUID().toString();
        RemoveCommentCommand sample = new RemoveCommentCommand();
        sample.commentId = commentId;
        return sample;
    }
}
