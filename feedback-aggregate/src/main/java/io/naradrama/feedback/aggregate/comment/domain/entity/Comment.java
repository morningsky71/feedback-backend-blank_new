/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package io.naradrama.feedback.aggregate.comment.domain.entity;

import io.naradrama.feedback.aggregate.comment.domain.entity.sdo.CommentCdo;
import io.naradrama.prologue.domain.NameValue;
import io.naradrama.prologue.domain.NameValueList;
import io.naradrama.prologue.domain.ddd.DomainAggregate;
import io.naradrama.prologue.domain.ddd.DomainEntity;
import io.naradrama.prologue.util.json.JsonUtil;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.beans.BeanUtils;

@Getter
@Setter
@NoArgsConstructor
public class Comment extends DomainEntity implements DomainAggregate {
    //
    private String writerId;
    private String writerName;
    private String message;
    private String base64AttachedImage;
    private String feedbackId;
    private boolean important;
    private boolean deleted;
    private long time;

    public Comment(String id) {
        //
        super(id);
    }

    public Comment(CommentCdo commentCdo) {
        super();
        BeanUtils.copyProperties(commentCdo, this);
        this.important = false;
        this.deleted = false;
        this.time = System.currentTimeMillis();
    }

    public void markDeleted() {
        //
        this.deleted = true;
    }

    public void modifyValues(NameValueList nameValues) {
        //
        for (NameValue nameValue : nameValues.list()) {
            String value = nameValue.getValue();
            switch (nameValue.getName()) {
                case "message":
                    this.message = value;
                    break;
                case "base64AttachedImage":
                    this.base64AttachedImage = value;
                    break;
                case "important":
                    this.important = Boolean.parseBoolean(value);
                    break;

                default:
                    throw new IllegalArgumentException("Update not allowed: " + nameValue);
            }
        }
    }

    @Override
    public String toString() {
        //
        return toJson();
    }

    public static Comment fromJson(String json) {
        //
        return JsonUtil.fromJson(json, Comment.class);
    }

    public static Comment sample() {
        //
        return new Comment(CommentCdo.sample());
    }

    public static void main(String[] args) {
        //
        System.out.println(sample().toPrettyJson());
    }
}
