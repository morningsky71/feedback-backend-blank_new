/* 
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package io.naradrama.feedback.aggregate.comment.store;

import io.naradrama.feedback.aggregate.comment.domain.entity.Comment;
import io.naradrama.prologue.domain.Offset;

import java.util.List;

public interface
CommentStore {
    /* Autogen by nara studio */
    void create(Comment comment);
    Comment retrieve(String id);
    List<Comment> retrieveAll(Offset offset);
    void update(Comment comment);
    void delete(Comment comment);
    void delete(String id);
    boolean exists(String id);

    List<Comment> retrieveAllByFeedbackId(String feedbackId);
    List<Comment> deleteByFeedbackId(String feedbackId);
}
