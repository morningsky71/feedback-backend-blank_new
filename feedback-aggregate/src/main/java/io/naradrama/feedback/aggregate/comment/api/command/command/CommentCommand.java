/* 
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package io.naradrama.feedback.aggregate.comment.api.command.command;

import lombok.Getter;
import lombok.Setter;
import lombok.NoArgsConstructor;
import io.naradrama.prologue.domain.cqrs.command.CqrsBaseCommand;
import io.naradrama.feedback.aggregate.comment.domain.entity.sdo.CommentCdo;
import java.util.List;
import java.lang.String;
import io.naradrama.prologue.domain.NameValueList;
import io.naradrama.prologue.domain.cqrs.command.CqrsBaseCommandType;
import io.naradrama.prologue.util.json.JsonUtil;

@Getter
@Setter
@NoArgsConstructor
public class CommentCommand extends CqrsBaseCommand {
    /* Autogen by nara studio */
    private CommentCdo commentCdo;
    private List<CommentCdo> commentCdos;
    private String commentId;
    private NameValueList nameValues;

    protected CommentCommand(CqrsBaseCommandType type) {
        /* Autogen by nara studio */
        super(type);
    }

    public static CommentCommand newRegisterCommentCommand(CommentCdo commentCdo) {
        /* Autogen by nara studio */
        CommentCommand command = new CommentCommand(CqrsBaseCommandType.Register);
        command.setCommentCdo(commentCdo);
        return command;
    }

    public static CommentCommand newRegisterCommentCommand(List<CommentCdo> commentCdos) {
        /* Autogen by nara studio */
        CommentCommand command = new CommentCommand(CqrsBaseCommandType.Register);
        command.setCommentCdos(commentCdos);
        return command;
    }

    public static CommentCommand newModifyCommentCommand(String commentId, NameValueList nameValues) {
        /* Autogen by nara studio */
        CommentCommand command = new CommentCommand(CqrsBaseCommandType.Modify);
        command.setCommentId(commentId);
        command.setNameValues(nameValues);
        return command;
    }

    public static CommentCommand newRemoveCommentCommand(String commentId) {
        /* Autogen by nara studio */
        CommentCommand command = new CommentCommand(CqrsBaseCommandType.Remove);
        command.setCommentId(commentId);
        return command;
    }

    public String toString() {
        /* Autogen by nara studio */
        return toJson();
    }

    public static CommentCommand fromJson(String json) {
        /* Autogen by nara studio */
        return JsonUtil.fromJson(json, CommentCommand.class);
    }
}
