/* 
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package io.naradrama.feedback.aggregate.comment.api.query.query;

import lombok.Getter;
import lombok.Setter;
import lombok.NoArgsConstructor;
import io.naradrama.prologue.domain.cqrs.query.CqrsDynamicQuery;
import io.naradrama.feedback.aggregate.comment.domain.entity.Comment;
import io.naradrama.prologue.util.query.RdbQueryRequest;
import io.naradrama.feedback.aggregate.comment.store.maria.jpo.CommentJpo;
import io.naradrama.prologue.util.query.RdbQueryBuilder;
import javax.persistence.TypedQuery;

@Getter
@Setter
@NoArgsConstructor
public class CommentDynamicQuery extends CqrsDynamicQuery<Comment> {
    /* Autogen by nara studio */

    public void execute(RdbQueryRequest<CommentJpo> request) {
        /* Autogen by nara studio */
        request.addQueryStringAndClass(genSqlString(), CommentJpo.class);
        TypedQuery<CommentJpo> query = RdbQueryBuilder.build(request);
        CommentJpo commentJpo = query.getSingleResult();
        setQueryResult(commentJpo.toDomain());
    }
}
