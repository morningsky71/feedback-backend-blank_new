/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package io.naradrama.feedback.aggregate.comment.store.maria.repository;

import io.naradrama.feedback.aggregate.comment.store.maria.jpo.CommentJpo;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CommentMariaRepository extends PagingAndSortingRepository<CommentJpo, String> {
    //
    // TODO
    //  1. Declare some methods which are required by CommentMariaStore

    void deleteByFeedbackId(String feedbackId);

}
