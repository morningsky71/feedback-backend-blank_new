/* 
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package io.naradrama.feedback.aggregate.comment.api.query.query;

import lombok.Getter;
import lombok.Setter;
import lombok.NoArgsConstructor;
import io.naradrama.prologue.domain.cqrs.query.CqrsBaseQuery;
import io.naradrama.feedback.aggregate.comment.domain.entity.Comment;
import java.lang.String;
import io.naradrama.feedback.aggregate.comment.store.CommentStore;

@Getter
@Setter
@NoArgsConstructor
public class CommentQuery extends CqrsBaseQuery<Comment> {
    /* Autogen by nara studio */
    private String commentId;

    public void execute(CommentStore commentStore) {
        /* Autogen by nara studio */
        setQueryResult(commentStore.retrieve(commentId));
    }
}
