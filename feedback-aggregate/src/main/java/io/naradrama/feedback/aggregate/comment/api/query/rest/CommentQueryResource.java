/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package io.naradrama.feedback.aggregate.comment.api.query.rest;

import io.naradrama.feedback.aggregate.comment.api.query.query.CommentDynamicQuery;
import io.naradrama.feedback.aggregate.comment.api.query.query.CommentQuery;
import io.naradrama.feedback.aggregate.comment.api.query.query.CommentsDynamicQuery;
import io.naradrama.feedback.aggregate.comment.store.CommentStore;
import io.naradrama.feedback.aggregate.comment.store.maria.jpo.CommentJpo;
import io.naradrama.prologue.util.query.RdbQueryRequest;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.persistence.EntityManager;

// TODO Implement RestController Component Class which implements CommentQueryFacade and receives CommentStore, EntityManager as Dependency Injection
//  url
//  1. root : /aggregate/comment/query
//  2. base query : /
//  3. dynamic query with single result : /dynamic-single
//  4. dynamic query with multiple results : /dynamic-multi

@RestController
@RequestMapping(value ="/aggregate/comment/query")
public class CommentQueryResource implements CommentQueryFacade{

    private final CommentStore commentStore;
    private final RdbQueryRequest<CommentJpo> request;

    public CommentQueryResource(CommentStore commentStore, EntityManager entityManager) {
        this.commentStore = commentStore;
        this.request = new RdbQueryRequest<>(entityManager);
    }

    @PostMapping(value="/")
    public CommentQuery execute(@RequestBody CommentQuery commentQuery) {

        try {
            commentQuery.execute(commentStore);
        }catch(Exception e){
            e.getMessage();
        }
        return commentQuery;
    }

    @PostMapping(value="/dynamic-single")
    public CommentDynamicQuery execute(@RequestBody CommentDynamicQuery  commentDynamicQuery)  {

        commentDynamicQuery.execute(request);
        return commentDynamicQuery;
    }

    @PostMapping(value="/dynamic-multi")
    public CommentsDynamicQuery execute(@RequestBody CommentsDynamicQuery  commentsDynamicQuery)  {

        commentsDynamicQuery.execute(request);
        return commentsDynamicQuery;
    }

}
