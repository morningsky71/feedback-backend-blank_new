/* 
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package io.naradrama.feedback.aggregate.comment.api.query.query;

import lombok.Getter;
import lombok.Setter;
import lombok.NoArgsConstructor;
import io.naradrama.prologue.domain.cqrs.query.CqrsDynamicQuery;
import java.util.List;
import io.naradrama.feedback.aggregate.comment.domain.entity.Comment;
import io.naradrama.prologue.util.query.RdbQueryRequest;
import io.naradrama.feedback.aggregate.comment.store.maria.jpo.CommentJpo;
import io.naradrama.prologue.util.query.RdbQueryBuilder;
import javax.persistence.TypedQuery;
import io.naradrama.prologue.domain.Offset;
import static java.util.Objects.nonNull;

@Getter
@Setter
@NoArgsConstructor
public class CommentsDynamicQuery extends CqrsDynamicQuery<List<Comment>> {
    /* Autogen by nara studio */

    public void execute(RdbQueryRequest<CommentJpo> request) {
        /* Autogen by nara studio */
        request.addQueryStringAndClass(genSqlString(), CommentJpo.class);
        Offset offset = getOffset();
        TypedQuery<CommentJpo> query = RdbQueryBuilder.build(request, offset);
        query.setFirstResult(offset.getOffset());
        query.setMaxResults(offset.getLimit());
        List<CommentJpo> commentJpos = query.getResultList();
        setQueryResult(CommentJpo.toDomains(commentJpos));
        if (nonNull(getOffset()) && getOffset().isTotalCountRequested()) {
            TypedQuery<Long> countQuery = RdbQueryBuilder.buildForCount(request);
            long totalCount = countQuery.getSingleResult();
            Offset countableOffset = getOffset();
            countableOffset.setTotalCount((int) totalCount);
            setOffset(countableOffset);
        }
    }
}
