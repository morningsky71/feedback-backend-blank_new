/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package io.naradrama.feedback.aggregate.comment.domain.logic;

import io.naradrama.feedback.aggregate.comment.api.command.command.CommentCommand;
import io.naradrama.feedback.aggregate.comment.domain.entity.Comment;
import io.naradrama.feedback.aggregate.comment.domain.entity.sdo.CommentCdo;
import io.naradrama.feedback.aggregate.comment.store.CommentStore;
import io.naradrama.prologue.domain.NameValue;
import io.naradrama.prologue.domain.NameValueList;
import io.naradrama.prologue.domain.Offset;
import io.naradrama.prologue.domain.cqrs.FailureMessage;
import io.naradrama.prologue.domain.cqrs.command.CommandResponse;
import io.naradrama.prologue.util.json.JsonUtil;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityNotFoundException;
import java.sql.Date;
import java.sql.SQLDataException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@Service
@Transactional
public class CommentLogic {
    // Autogen by nara studio
    private final CommentStore commentStore;

    public CommentLogic(CommentStore commentStore) {
        /* Autogen by nara studio */
        this.commentStore = commentStore;
    }

    public CommentCommand routeCommand(CommentCommand command) throws Exception {

        try {
            switch (/* Autogen by nara studio */
                    command.getCqrsBaseCommandType()) {
                case Register:
                    if (command.getCommentCdos().size() > 0) {
                        List<String> entityIds = this.registerComments(command.getCommentCdos());
                        command.setCommandResponse(new CommandResponse(entityIds));
                    } else {
                        String entityId = this.registerComment(command.getCommentCdo());
                        command.setCommandResponse(new CommandResponse(entityId));
                    }
                    break;
                case Modify:
                    this.modifyComment(command.getCommentId(), command.getNameValues());
                    command.setCommandResponse(new CommandResponse(command.getCommentId()));
                    break;
                case Remove:
                    this.removeComment(command.getCommentId());
                    command.setCommandResponse(new CommandResponse(command.getCommentId()));
                    break;
                default:
                    command.setFailureMessage(new FailureMessage(new Throwable("CommandType must be Register, Modify or Remove")));
            }

        }catch(Exception e){
            throw e;
        }
        return command;

    }

    public String registerComment(CommentCdo commentCdo) throws Exception {

        String id = null;

        //
        // TODO
        //  1. Save entity from cdo when entity was not exists
        //  2. Return registered entity's id

        Comment newComment = new Comment(commentCdo);
        Comment oldComment = commentStore.retrieve(newComment.getId());

        if (oldComment ==null) {
            commentStore.create(newComment);
            id =newComment.getId();
        }else{
            throw new Exception("already data found" + JsonUtil.toJson(commentCdo));
        }

        return id;

    }

    public List<String> registerComments(List<CommentCdo> commentCdos) throws Exception {

        //
        // TODO
        //  1. Register entities from cdo list
        //  2. Return registered entity's id list

        List<String> registeredComments = new ArrayList<>();
        String registeredCommentId = null;

        Iterator<CommentCdo> itr = commentCdos.iterator();
        while( itr.hasNext() )
        {
            registeredCommentId = this.registerComment(itr.next());
            if(registeredCommentId !=null){
                registeredComments.add(registeredCommentId);
            }
        }
        return registeredComments;
    }

    public Comment findComment(String commentId) {

        // TODO
        //  1. Find entity by optimal query condition
        //  2. If result was empty, throw exception

        Comment comment = null;
        comment = commentStore.retrieve(commentId);

        if (comment ==null) {
              throw new EntityNotFoundException("record dose not found."+ commentId);
        }

        return comment;
    }

    public List<Comment> findAllComment(Offset offset) {

        // TODO
        //  1. Find entities by optimal query conditions
        commentStore.retrieveAll(offset);
        return null;
    }

    public void modifyComment(String commentId, NameValueList nameValues) {

        //
        // TODO
        //  1. Modify entity
         Comment comment = commentStore.retrieve(commentId);

        if (nameValues.getValueOf("message") != null) {
            comment.setMessage(nameValues.getValueOf("message"));
        }
        if (nameValues.getValueOf("base64AttachedImage") != null) {
            comment.setBase64AttachedImage(nameValues.getValueOf("base64AttachedImage"));
        }
        if (nameValues.getValueOf("feedbackId") != null) {
            comment.setFeedbackId(nameValues.getValueOf("feedbackId"));
        }
        if (nameValues.getValueOf("deleted") != null) {
            comment.setDeleted(Boolean.valueOf(nameValues.getValueOf("deleted")));
        }
        if (nameValues.getValueOf("time") != null) {
            comment.setTime(Date.valueOf(nameValues.getValueOf("time")).getTime());
        }

        commentStore.update(comment);

    }

    public void modifyComment(Comment comment) {
        //
        // TODO
        //  1. Modify entity
        commentStore.update(comment);
    }

    public void removeComment(String commentId) {
        //
        // TODO
        //  1. Remove entity
        commentStore.delete(commentId);
    }

    public boolean exists(String commentId) {
        //
        // TODO
        //  1. Check existence
        return commentStore.exists(commentId);
    }
}
