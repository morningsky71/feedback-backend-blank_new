/* 
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package io.naradrama.feedback.aggregate.comment.store.maria.jpo;

import lombok.Getter;
import lombok.Setter;
import lombok.NoArgsConstructor;
import javax.persistence.Entity;
import javax.persistence.Table;
import io.naradrama.prologue.store.jpa.DomainEntityJpo;
import java.lang.String;
import io.naradrama.feedback.aggregate.comment.domain.entity.Comment;
import org.springframework.beans.BeanUtils;
import io.naradrama.prologue.util.json.JsonUtil;
import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "COMMENT")
public class CommentJpo extends DomainEntityJpo {
    /* Autogen by nara studio */
    private String writerId; // 
    private String writerName;
    private String message;
    private String base64AttachedImage;
    private String feedbackId;
    private boolean important;
    private boolean deleted;
    private long time;

    public CommentJpo(Comment comment) {
        /* Autogen by nara studio */
        super(comment);
        BeanUtils.copyProperties(comment, this);
    }

    public Comment toDomain() {
        /* Autogen by nara studio */
        Comment comment = new Comment(getId());
        BeanUtils.copyProperties(this, comment);
        return comment;
    }

    public static List<Comment> toDomains(List<CommentJpo> commentJpos) {
        /* Autogen by nara studio */
        return commentJpos.stream().map(CommentJpo::toDomain).collect(Collectors.toList());
    }

    public String toString() {
        /* Autogen by nara studio */
        return toJson();
    }

    public static CommentJpo sample() {
        /* Autogen by nara studio */
        return new CommentJpo(Comment.sample());
    }

    public static void main(String[] args) {
        /* Autogen by nara studio */
        System.out.println(sample());
    }
}
