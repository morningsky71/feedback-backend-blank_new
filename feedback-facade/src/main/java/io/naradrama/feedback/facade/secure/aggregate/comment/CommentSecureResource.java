/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package io.naradrama.feedback.facade.secure.aggregate.comment;

import io.naradrama.feedback.aggregate.comment.api.command.command.CommentCommand;
import io.naradrama.feedback.aggregate.comment.api.command.rest.CommentFacade;
import io.naradrama.feedback.aggregate.comment.domain.logic.CommentLogic;
import io.naraplatform.daysman.daysboy.config.Daysboy;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

// TODO Extract Apis from CommentResource which are used by front project
//  url
//   1.replace 'aggregate' to 'secure' of origin url (e.g. /aggregate/comment/~ => /secure/comment/~)
@RestController
@RequestMapping(value ="/secure/comment")
public class CommentSecureResource implements CommentFacade {


    private final CommentLogic commentLogic;

    public CommentSecureResource(CommentLogic commentLogic) {
        this.commentLogic = commentLogic;
    }

    @Daysboy
    @PostMapping(value="/comment/command")
    @Override
    public CommentCommand executeComment(@RequestBody CommentCommand commentCommand) {

        CommentCommand ret = null;

        try {
            ret =  commentLogic.routeCommand(commentCommand);
        }catch(Exception e){
             e.getMessage();
        }

        return ret;
    }
}
