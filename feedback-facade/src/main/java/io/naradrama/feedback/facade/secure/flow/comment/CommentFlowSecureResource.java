/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package io.naradrama.feedback.facade.secure.flow.comment;

import io.naradrama.feedback.flow.comment.api.command.command.RegisterCommentCommand;
import io.naradrama.feedback.flow.comment.api.command.command.RemoveCommentCommand;
import io.naradrama.feedback.flow.comment.api.command.rest.CommentFlowFacade;
import io.naradrama.feedback.flow.comment.domain.logic.CommentFlowLogic;
import io.naraplatform.daysman.daysboy.config.Daysboy;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

// TODO Extract Apis from CommentFlowResource which are used by front project
//  url
//   1. replace 'flow' to 'secure' of origin url (e.g. /flow/comment-flow/~ => /secure/comment-flow/~)

@RestController
@RequestMapping("/secure/comment-flow")
public class CommentFlowSecureResource implements CommentFlowFacade {

    //
    private final CommentFlowLogic commentFlowLogic;

    public CommentFlowSecureResource(CommentFlowLogic commentFlowLogic) {
        this.commentFlowLogic = commentFlowLogic;
    }

    @Daysboy
    @Override
    @PostMapping("/register-comment")
    public RegisterCommentCommand registerComment(@RequestBody RegisterCommentCommand command) {
        //
        return commentFlowLogic.registerComment(command);
    }

    @Daysboy
    @Override
    @PostMapping("/remove-comment")
    public RemoveCommentCommand removeComment(@RequestBody RemoveCommentCommand command) {
        //
        return commentFlowLogic.removeComment(command);
    }

}
